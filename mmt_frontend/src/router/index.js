import Vue from "vue";
import VueRouter from "vue-router";
import goTo from "vuetify/lib/services/goto";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/auth/Login"),
  },
  {
    path: "/dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/layouts/Dashboard"),
    children: [
      {
        path: "/",
        name: "Dashboard",
        component: () =>
          import(
            /* webpackChunkName: "dashboard/home" */ "../views/dashboard/Home"
          ),
      },
      {
        path: "/experiments",
        name: "Experiments",
        component: () =>
          import(
            /* webpackChunkName: "dashboard/experiments" */ "../views/dashboard/Experiments"
          ),
      },
      {
        path: "/toolbox",
        name: "Toolbox",
        component: () =>
          import(
            /* webpackChunkName: "dashboard/toolbox" */ "../views/dashboard/Toolbox"
          ),
      },
      {
        path: "/users",
        name: "Users",
        component: () =>
          import(
            /* webpackChunkName: "dashboard/users" */ "../views/dashboard/Users"
          ),
      },
    ],
  },
];
const scrollBehavior = (to, from, savedPosition) => {
  let scrollTo = 0;

  if (to.hash) {
    scrollTo = to.hash;
  } else if (savedPosition) {
    scrollTo = savedPosition.y;
  }

  return goTo(scrollTo);
};

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior,
  routes,
});

export default router;
