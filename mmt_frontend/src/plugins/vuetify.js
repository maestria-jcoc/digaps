import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader
import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import es from "vuetify/lib/locale/es";
import en from "vuetify/lib/locale/en";
import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { es, en },
    current: "es",
  },
  theme: {
    themes: {
      light: {
        primary: "#1B396A",
        secondary: "#343a40",
        accent: "#B38E5D",
        error: colors.red.base,
        warning: colors.amber.base,
        info: colors.teal.base,
        success: colors.green.base,
      },
    },
  },
});
