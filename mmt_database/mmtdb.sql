CREATE SCHEMA IF NOT EXISTS mmtdb;
USE mmtdb;

DROP TABLE IF EXISTS wfd_impedance_measurements;
DROP TABLE IF EXISTS wfd_temperature_measurements;
DROP TABLE IF EXISTS wfd_calibrations;
DROP TABLE IF EXISTS infiltrometer_pressure_measurements;
DROP TABLE IF EXISTS infiltrometer_calibrations;
DROP TABLE IF EXISTS hygrometer_humidity_measurements;
DROP TABLE IF EXISTS hygrometer_resistivity_measurements;
DROP TABLE IF EXISTS hygrometers;
DROP TABLE IF EXISTS wetting_front_detectors;
DROP TABLE IF EXISTS infiltrometers;
DROP TABLE IF EXISTS experiments;
DROP TABLE IF EXISTS users;

create table users
(
    id         bigint unsigned auto_increment,
    name       varchar(100) null,
    email      varchar(100) not null,
    password   varchar(100) not null,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp    null,
    deleted_at timestamp    null,
    constraint users_pk
        primary key (id)
) ENGINE = InnoDB;
create table experiments
(
    id            bigint unsigned auto_increment,
    name          varchar(50) not null,
    description   text        null,
    sampling_rate int         not null,
    frequency     int         not null,
    status        bool      default false,
    created_at    timestamp default CURRENT_TIMESTAMP,
    updated_at    timestamp   null,
    deleted_at    timestamp   null,
    user_id       bigint unsigned,
    constraint experiments_pk
        primary key (id),
    constraint experiments_users__fk
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table wetting_front_detectors
(
    id           bigint unsigned auto_increment,
    description  varchar(50) not null,
    sensor_count tinyint     not null,
    created_at   timestamp default CURRENT_TIMESTAMP,
    updated_at   timestamp   null,
    deleted_at   timestamp   null,
    constraint wetting_front_detectors_pk
        primary key (id)
) ENGINE = InnoDB;
create table infiltrometers
(
    id          bigint unsigned auto_increment,
    description varchar(50) not null,
    created_at  timestamp default CURRENT_TIMESTAMP,
    updated_at  timestamp   null,
    deleted_at  timestamp   null,
    constraint infiltrometers_pk
        primary key (id)
) ENGINE = InnoDB;
create table hygrometers
(
    id           bigint unsigned auto_increment,
    description  varchar(50) not null,
    sensor_count tinyint     not null,
    created_at   timestamp default CURRENT_TIMESTAMP,
    updated_at   timestamp   null,
    deleted_at   timestamp   null,
    constraint hygrometers_pk
        primary key (id)
) ENGINE = InnoDB;
create table wfd_impedance_measurements
(
    id                        bigint unsigned auto_increment,
    real_part                 float   not null,
    imaginary_part            float   not null,
    sensor_number             tinyint null comment 'WFD sensor array number',
    created_at                timestamp default CURRENT_TIMESTAMP,
    wetting_front_detector_id bigint unsigned,
    experiment_id             bigint unsigned,
    constraint wfd_impedance_measurements_pk
        primary key (id),
    constraint wfd_impedance_measurements_wetting_front_detectors__fk
        foreign key (wetting_front_detector_id) references wetting_front_detectors (id)
            on update cascade on delete cascade,
    constraint wfd_impedance_measurements_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table wfd_temperature_measurements
(
    id                        bigint unsigned auto_increment,
    value                     float   not null,
    sensor_number             tinyint null comment 'WFD sensor array number',
    created_at                timestamp default CURRENT_TIMESTAMP,
    wetting_front_detector_id bigint unsigned,
    experiment_id             bigint unsigned,
    constraint wfd_temperature_measurements_pk
        primary key (id),
    constraint wfd_temperature_measurements_wetting_front_detectors__fk
        foreign key (wetting_front_detector_id) references wetting_front_detectors (id)
            on update cascade on delete cascade,
    constraint wfd_temperature_measurements_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table wfd_calibrations
(
    id                        bigint unsigned auto_increment,
    real_part                 float     not null,
    imaginary_part            float     not null,
    created_at                timestamp not null default CURRENT_TIMESTAMP,
    sensor_number             tinyint   null comment 'WFD sensor array number',
    wetting_front_detector_id bigint unsigned,
    experiment_id             bigint unsigned,
    constraint wfd_calibrations_pk
        primary key (id),
    constraint wfd_calibrations_wetting_front_detectors__fk
        foreign key (wetting_front_detector_id) references wetting_front_detectors (id)
            on update cascade on delete cascade,
    constraint wfd_calibrations_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table infiltrometer_pressure_measurements
(
    id               bigint unsigned auto_increment,
    pressure_height  float     not null,
    created_at       timestamp not null default CURRENT_TIMESTAMP,
    infiltrometer_id bigint unsigned,
    experiment_id    bigint unsigned,
    constraint infiltrometer_pressure_measurements_pk
        primary key (id),
    constraint infiltrometer_pressure_measurements_infiltrometers__fk
        foreign key (infiltrometer_id) references infiltrometers (id)
            on update cascade on delete cascade,
    constraint infiltrometer_pressure_measurements_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table infiltrometer_calibrations
(
    id               bigint unsigned auto_increment,
    span             float     not null,
    zero             float     not null,
    created_at       timestamp not null default CURRENT_TIMESTAMP,
    infiltrometer_id bigint unsigned,
    experiment_id    bigint unsigned,
    constraint infiltrometer_calibrations_pk
        primary key (id),
    constraint infiltrometer_calibrations_infiltrometers__fk
        foreign key (infiltrometer_id) references infiltrometers (id)
            on update cascade on delete cascade,
    constraint infiltrometer_calibrations_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table hygrometer_humidity_measurements
(
    id            bigint unsigned auto_increment,
    value         float   not null,
    sensor_number tinyint null comment 'WFD sensor array number',
    created_at    timestamp default CURRENT_TIMESTAMP,
    hygrometer_id bigint unsigned,
    experiment_id bigint unsigned,
    constraint hygrometer_humidity_measurements_pk
        primary key (id),
    constraint hygrometer_humidity_measurements_hygrometers__fk
        foreign key (hygrometer_id) references hygrometers (id)
            on update cascade on delete cascade,
    constraint hygrometer_humidity_measurements_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;
create table hygrometer_resistivity_measurements
(
    id            bigint unsigned auto_increment,
    value         float not null,
    created_at    timestamp default CURRENT_TIMESTAMP,
    hygrometer_id bigint unsigned,
    experiment_id bigint unsigned,
    constraint hygrometer_resistivity_measurements_pk
        primary key (id),
    constraint hygrometer_resistivity_measurements_hygrometers__fk
        foreign key (hygrometer_id) references hygrometers (id)
            on update cascade on delete cascade,
    constraint hygrometer_resistivity_measurements_experiments__fk
        foreign key (experiment_id) references experiments (id)
            on update cascade on delete cascade
) ENGINE = InnoDB;

create unique index users_email_uindex
    on users (email);

